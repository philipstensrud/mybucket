# Code School: Git Real

Jane modifies README.txt file & adds license.

```
$ git status
# On branch master
# Changed but not updated:
#
# modified: README.txt
#
# Untracked files:
#
# LICENSE
no changes added to commit
```

Add both files to staging area

```
$ git add README.txt LICENSE
```
OR
```
$ git add --all
```
```
$ git status
# On branch master
# Changes to be commited:
#
# new file: LICENSE
# modified: README.md
```

### Different ways to Add

__Add the list of file__
```
$ git add <list of files>
```
__Add all files__
```
$ git add --all
```
__Add all txt files in current directory__
```
$ git add *.txt
```
__Add all txt files in docs directory__
```
$ git add docs/*.txt
```
__Add all files in docs directory__
```
$ git add docs/
```
__Add all txt files in the whole project__
```
$ git add "*.txt"
```

### VIEWING STAGED DIFFERENCES

__Show difference between staged and non-staged__
```
$ git diff
```
__View staged differences__
```
$ git diff --staged
```

__Adding a new line__
New line since last time.

__Backing up__
```
git reset HEAD .\README.md
git checkout -- .\README.md
```

## mod

her legger vi til noe.

## Branches some updates

I hope this isn't a problem.

Will add bitbucket to repo  
